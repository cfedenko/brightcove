# Copyright © 2013 Constantine Fedenko <cfedenko@gmail.com>
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://www.wtfpl.net/ for more details.

import urllib
import urllib2
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers
try:
    import simplejson as json
except ImportError:
    import json

class BrightcoveError(Exception):
    pass

class Brightcove(object):
    read_url = 'http://api.brightcove.com/services/library'
    write_url = 'http://api.brightcove.com/services/post'

    def __init__(self, read_token=None, write_token=None):
        super(Brightcove, self).__init__()
        self.read_token = read_token
        self.write_token = write_token

    def read(self, method, params=None):
        _params = {
            'command': method,
            'token': self.read_token
        }
        if params:
            _params.update(params)
        data = urllib.urlencode(_params)
        request = urllib2.Request(self.read_url, data)
        response = urllib2.urlopen(request)
        response = json.load(response)
        #TODO
        return response

    def write(self, method, params=None, file_to_upload=None):
        _params = {'token': self.write_token}
        if params:
            _params.update(params)
        data = {
            'method': method,
            'params': _params
        }
        if file_to_upload:
            register_openers()
            datagen, headers = multipart_encode({
                'json': json.dumps(data),
                "file": open(file_to_upload, "rb")
            })
            request = urllib2.Request(self.write_url, datagen, headers)
        else:
            data = urllib.urlencode({'json': json.dumps(data)})
            request = urllib2.Request(self.write_url, data)
        response = urllib2.urlopen(request)
        response = json.load(response)
        if 'error' in response and response['error']:
            raise BrightcoveError(response['error']['message'])
        return response['result']
